module.exports = function (app, connection) {

    /**
     * @api {get} /exports/matches?round=[1/4,1/2,Final] Get a .csv export for all the matches of a round
     * @apiGroup Exports
     *
     * @apiParam {string} round 1/4 : supermatchs des 1/4<br/>
     *                          1/2 : supermatchs des demis<br/>
     *                          Final : supermatchs de la finale<br/>
     *
     *  @apiParamExample Request-Example:
     *  /exports/matches?round=1/4
     *
     * @apiSuccessExample Success-Response:
     *
     * a .csv file
     *
     * @apiErrorExample
     *
     * {
     *  code = -1 (or -2)
     * }
     */
    app.route("/exports/matches")
        .get(function (req, res) {
            var o = {};
            if (typeof req.query.round === "undefined") {
                o.code = -1;
                o.message = "Require 'round' param";
                res.send(JSON.stringify(o, null, 4));
            } else {
                connection.query("CALL InfosHugeMatchs(?);", [req.query.round], function (err, rows) {
                    if (err != null) {
                        o.code = -2;
                        o.message = "Database error (bad round value)";
                        res.send(JSON.stringify(o, null, 4));
                    } else {
                        var matches = rows[0];
                        var matchesCount = matches.length;
                        var expMatches = [];
                        for (var i = 0; i < matchesCount; i++) {
                            var match = matches[i];
                            var expMatch = {};
                            expMatch.Id_Match = match.IdMatch;
                            expMatch.Round = getRoundName(match.Tableau);
                            expMatch.Category = getCategoryName(match.Category);
                            expMatch.Team_A = match.Team_A;
                            expMatch.Team_B = match.Team_B;
                            expMatch.NameTeam_A = match.NameTeam_A;
                            expMatch.NameTeam_B = match.NameTeam_B;
                            expMatch.Player_A1 = match.Player_A1;
                            expMatch.Player_A2 = match.Player_A2;
                            expMatch.Player_B1 = match.Player_B1;
                            expMatch.Player_B2 = match.Player_B2;
                            expMatch.Set_1 = getSetScore(match, 1);
                            expMatch.Set_2 = getSetScore(match, 2);
                            expMatch.Set_3 = getSetScore(match, 3);
                            expMatch.Winner = getWinner(match);
                            console.log(match.IdMatch);
                            expMatches.push(expMatch);
                        }
                        app.ACconverter.json2csv(expMatches, function (err, csv) {
                            if (err) throw err;
                            res.header("Content-Type", "text/csv");
                            res.header("Content-disposition", "attachment; filename=matches" + req.query.round + ".csv");
                            res.send(csv);
                        });
                    }
                });
            }
        });

    function getRoundName(code) {
        if (code == "1/4-P")
            return "Quarterfinals Main";
        if (code == "1/2-P")
            return "Semifinals Main";
        if (code == "Final-P")
            return "Final Main";
        if (code == "1/2-5")
            return "Semifinals 5th place";
        if (code == "Final-3")
            return "Final 3rd place";
        if (code == "Final-5")
            return "Final 5th place";
        if (code == "Final-7")
            return "Final 7th place";
        return "";
    }

    function getCategoryName(code) {
        if (code == "SM")
            return "Men's Singles";
        if (code == "SW")
            return "Women's Singles";
        if (code == "DM")
            return "Men's Doubles";
        if (code == "DW")
            return "Women's Doubles";
        if (code == "DX")
            return "Mixed Doubles";
        return "";
    }

    function getSetScore(match, setId) {
        if (match["SetA" + setId] == null || match["SetA" + setId] == -1) {
            return ""
        } else {
            return match["SetA" + setId] + "-" + match["SetB" + setId]
        }
    }

    function getWinner(match) {
        if (match.Winner == "A") {
            return match.NameTeam_A;
        } else if (match.Winner == "B") {
            return match.NameTeam_B;
        } else {
            return "";
        }
    }
};