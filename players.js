module.exports = function (app, connection, error) {

	app.route("/aze").post(function (req, res){
		console.log(req.body);
		res.send("lol");
	});

    app.route("/players")
    /**
     * @api {get} /players Request the list of players
     * @apiGroup Players
     *
     * @apiSuccessExample Success-Response:
     {
    "code": 0,
    "players": [
        {
            "IdPlayer": 1,
            "Team": "BEL",
            "Name": "CANT",
            "FirstName": "Louis",
            "Sex": "M",
            "DateBirth": "1990-11-08",
            "Url": "https://media.licdn.com/mpr/mpr/shrink_120_120/p/2/005/099/261/0a7ab72.jpg"
        },
        {
            "IdPlayer": 2,
            "Team": "BEL",
            "Name": "STORME",
            "FirstName": "James Junior",
            "Sex": "M",
            "DateBirth": "1994-12-13",
            "Url": null
        }
        ]
     }
     */
        .get(function (req, res) {
            connection.query("CALL ListsPlayers();", function (err, rows) {
                var o = {};
                if (err != null) {
                    o.code = -2;
                } else {
                    o.code = 0;
                    if (Array.isArray(rows))
                        o.players = rows[0];
                    else
                        o.players = [];
                }
                res.send(JSON.stringify(o, null, 4));
            });
        })
    /**
     * @api {post} /players Creates a new player
     * @apiGroup Players
     * @apiParamExample Request-Example:
     {
        "name": "Elon",
        "firstname": "Musk",
        "sex": "M",
        "birthdate": "1995-02-03",
        "team": "USA",
        "nationalRankingSingle" : 12,
        "nationalRankingDouble" : 2,
        "atp_wta_rankingSingle" : null,
        "atp_wta_rankingDouble" : null
     }
     * @apiSuccessExample Success-Response:
     {
        "code": 0,
        "playerId": 56
     }
     */
        .post(function (req, res) {
            var o = {};
            if (req.body.name &&
                req.body.firstname &&
                req.body.sex &&
                req.body.birthdate &&
                req.body.team &&
                (req.body.nationalRankingSingle || req.body.nationalRankingSingle == null) &&
                (req.body.nationalRankingDouble || req.body.nationalRankingDouble == null) &&
                (req.body.atp_wta_rankingSingle || req.body.atp_wta_rankingSingle == null) &&
                (req.body.atp_wta_rankingSingle || req.body.atp_wta_rankingDouble == null)) {
                // Params OK
                connection.query("CALL AdminInsertPlayer(?,?,?,?,?,?,?,?,?);", [req.body.team, req.body.name, req.body.firstname,
                    req.body.sex, req.body.birthdate, req.body.nationalRankingSingle, req.body.nationalRankingDouble,
                    req.body.atp_wta_rankingSingle, req.body.atp_wta_rankingDouble], function (err, rows) {
                    if (err != null) {
                        o.code = -2;
                    } else {
                        if (Array.isArray(rows)) {
                            o.code = 0;
                            o.playerId = rows[0][0].IdPlayer;
                        }
                        else {
                            o.code = -3;
                        }
                    }
                    res.send(JSON.stringify(o, null, 4));
                });
            } else {
                o.code = -1;
                res.send(JSON.stringify(o, null, 4));
            }
        })
    /**
     * @api {put} /players Updates an existing player
     * @apiGroup Players
     * @apiParamExample Request-Example:
     {
        "playerId": 52,
        "name": "Elon",
        "firstname": "Musk",
        "sex": "M",
        "birthdate": "1995-02-03",
        "team": "USA",
        "nationalRankingSingle" : 12,
        "nationalRankingDouble" : null,
        "atp_wta_rankingSingle" : 134,
        "atp_wta_rankingDouble" : null
     }
     * @apiSuccessExample Success-Response:
     * { "code": 0 }
     */
        .put(function (req, res) {
            var o = {};
            if (req.body.name &&
                req.body.firstname &&
                req.body.sex &&
                req.body.birthdate &&
                req.body.team &&
                req.body.playerId &&
                (req.body.nationalRankingSingle || req.body.nationalRankingSingle == null) &&
                (req.body.nationalRankingDouble || req.body.nationalRankingDouble == null) &&
                (req.body.atp_wta_rankingSingle || req.body.atp_wta_rankingSingle == null) &&
                (req.body.atp_wta_rankingSingle || req.body.atp_wta_rankingDouble == null)) {
                // Params OK
                connection.query("CALL AdminUpdatePlayer(?,?,?,?,?,?,?,?,?,?);", [req.body.playerId, req.body.team, req.body.name,
                    req.body.firstname, req.body.sex, req.body.birthdate, req.body.nationalRankingSingle, req.body.nationalRankingDouble,
                    req.body.atp_wta_rankingSingle, req.body.atp_wta_rankingDouble], function (err, rows) {
                    if (err != null) {
                        o.code = -2;
                    } else {
                        o.code = 0;
                    }
                    res.send(JSON.stringify(o, null, 4));
                });
            } else {
                o.code = -1;
                res.send(JSON.stringify(o, null, 4));
            }
        });

    app.route("/players/:playerId")
    /**
     * @api {get} /players/:playerId Get the infomations about a player
     * @apiGroup Players
     *
     * @apiSuccessExample Success-Response:
     {
    "code": 0,
    "player": {
        "IdPlayer": 1,
        "Team": "TH",
        "Name": "a",
        "FirstName": "b",
        "Sex": "M",
        "DateBirth": "2015-03-28",
        "Url": "http://5.196.21.161/photos/photo_player_1.jpg"
    }
    }
     @apiSuccess {Integer} code
     0 : OK <br/>
     -2 : DB error<br/>
     -3 : the player does not exists
     *
     */
        .get(function (req, res) {
            connection.query("CALL InfosPlayer(?);", [req.params.playerId], function (err, rows) {
                var o = {};
                if (err == null) {
                    o.code = 0;
                    o.player = rows[0][0];
                } else if (err.sqlState == 45001) {
                    o.code = -3;
                    o.message = "Player doesn't exist";
                } else {
                    o.code = -2;
                }
                res.send(JSON.stringify(o, null, 4));
            });
        })
    /**
     * @api {delete} /players/:playerId Delete a player
     * @apiGroup Players
     *
     * @apiSuccessExample Success-Response:
     {"code":0}
     @apiSuccess {Integer} code
     0 : OK <br/>
     -2 : DB error<br/>
     -3 : Tournament in progress<br/>
     -4 : the player does not exists
     *
     */
        .delete(function (req, res) {
            connection.query("CALL DeletePlayer(?);", [req.params.playerId], function (err, rows) {
                var o = {};
                if (err == null) {
                    o.code = 0;
                } else if (err.sqlState == 45001) {
                    o.code = -3; // Tournament in progress
                } else if (err.sqlState == 45000) {
                    o.code = -4; // Player does not exists
                } else {
                    o.code = -2;
                }
                res.send(JSON.stringify(o, null, 4));
            });
        });

    app.route('/players/:playerId/photo')
    /**
     * @api {put} /players/:playerId/photo Upload and update a player's photo
     * @apiGroup Players
     * @apiParamExample Request-Example:
     Content-Type must be 'multipart/form-data'
     The photo file must be in the 'photo' field
     Accepted format : jpg, jpeg, png
     Max file size (in bytes) : 1000000
     * @apiSuccessExample Success-Response:
     {
        code: 0
        photoUrl: "http://5.196.21.161/photos/photo_player_1.jpg"
    }
     @apiSuccess {Integer} code
     0 : OK <br/>
     -2 : DB error<br/>
     -3 : file size limit exceeded<br/>
     -4 : no photo<br/>
     -5 : Player doesn't exists
     *
     */
        .put([app.ACmulter({
            dest: app.ACconfig.photos_directory_path,
            limits: {
                fieldSize: 2000000,
                files: 1,
                fileSize: 2000000
            },
            rename: function (fieldname, filename, req, resp) {
                return "photo_player_" + req.params.playerId;
            },
            onFileUploadStart: function (file) {
                if (file.fieldname != "photo") // Authorize only the upload with the filedname 'photo'
                    return false;
                if (file.extension.toLowerCase() != "jpeg" && file.extension.toLowerCase() != "jpg"
                    && file.extension.toLowerCase() != "png")
                    return false;
                file.uploadOK = true;
            },
            onFileSizeLimit: function (file) {
                app.ACfs.unlink(file.path); // delete the partially written file
                file.uploadOK = false;
            }
        }), function (req, res) {
            var o = {};
            if (!req.files.photo) {
                o.code = -4;
                o.message = "no photo";
                res.send(JSON.stringify(o, null, 4));
            } else if (req.files.photo.uploadOK) {
                app.AClwip.open(req.files.photo.path, function(err, image){
                    image.cover(300, 400, "lanczos", function(err, image){
                        if(err){
                            o.code = -1;
                            o.message = "resize error";
                            res.send(JSON.stringify(o, null, 4));
                        } else {
                            image.writeFile(req.files.photo.path, function(err){
                                if(err)
                                    console.log(err);
                                var photoUrl = app.ACconfig.photos_directory_url + req.files.photo.name;
                                connection.query("CALL SetUrlPlayer(?,?);", [req.params.playerId, photoUrl], function (err, rows) {
                                    if (err == null) {
                                        o.code = 0;
                                        o.photoUrl = photoUrl;
                                    } else if (err.sqlState == 45001) {
                                        app.ACfs.unlink(req.files.photo.path);
                                        o.code = -5;
                                        o.message = "Player doesn't exists";
                                    } else {
                                        app.ACfs.unlink(req.files.photo.path);
                                        o.code = -2;
                                    }
                                    res.send(JSON.stringify(o, null, 4));
                                });
                            });
                        }
                    });
                });
            } else {
                o.code = -3;
                o.message = "file size limit exceeded";
                res.send(JSON.stringify(o, null, 4));
            }
        }]);


    app.route("/players/:playerId/matches")
    /**
     * @api {get} /players/:playerId/matches Get the matches played by a player
     * @apiGroup Players
     *
     * @apiSuccessExample Success-Response:
     {
     "code": 0,
     "matches": [
         {
             "matchId": 23,
             "category": "SM",
             "court": 1,
             "tableau": "1/4-P",
             "winner": "B",
             "duree": "00:00",
             "sets": [
                 {
                     "A": 0,
                     "B": 0
                 }
             ],
             "scoreA": "30",
             "teamISOA": "TH",
             "teamNameA": "Thailand",
             "teamFlagA": "http://5.196.21.161/drapeaux/th.png",
             "playerIdA1": 1,
             "playerNameA1": "b a",
             "playerPhotoA1": "http://5.196.21.161/photos/photo_player_1.jpg",
             "playerIdA2": null,
             "playerNameA2": null,
             "playerPhotoA2": null,
             "scoreB": "0",
             "teamISOB": "AO",
             "teamNameB": "Angola",
             "teamFlagB": "http://5.196.21.161/drapeaux/ao.png",
             "playerIdB1": 10,
             "playerNameB1": "Lahn GABI",
             "playerPhotoB1": null,
             "playerIdB2": null,
             "playerNameB2": null,
             "playerPhotoB2": null
         },...
         ]
     }
     @apiSuccess {Integer} code
     0 : OK <br/>
     -2 : DB error<br/>
     -3 : the player does not exists
     *
     */
        .get(function (req, res) {
            connection.query("CALL GetPlayersMatches(?);", [req.params.playerId], function (err, rows) {
                var o = {};
                if (err == null) {
                    o.code = 0;
                    var matches = {};
                    if (Array.isArray(rows)) {
                        for (var j = 0; j < rows.length - 1; j++) {
                            var arrayLength = rows[j].length;
                            for (var i = 0; i < arrayLength; i++) {
                                buildPlayersMatches(matches, rows[j][i]);
                            }
                        }
                    }
                    // litteral object => array
                    o.matches = Object.keys(matches).map(function (key) {
                        return matches[key]
                    });
                } else if (err.sqlState == 45000) {
                    o.code = -3;
                    o.message = "Player doesn't exist";
                } else {
                    o.code = -2;
                }
                res.send(JSON.stringify(o, null, 4));
            });
        });

    function buildPlayersMatches(matches, row) {
        //console.log(row);
        if (!matches[row.IdMatch]) {
            var newMatch = {};
            newMatch.matchId = row.IdMatch;
            newMatch.category = row.Category;
            newMatch.court = row.Court;
            newMatch.tableau = row.Tableau;
            newMatch.winner = row.Winner;
            newMatch.duree = row.Duree;
            newMatch.sets = [];
            matches[row.IdMatch] = newMatch;
        }
        var match = matches[row.IdMatch];
        var ref = row.Ref;
        match["score" + ref] = row.Score;
        match["teamISO" + ref] = row.Team;
        match["teamName" + ref] = row.NameTeam;
        match["teamFlag" + ref] = row.UrlTeam;
        match["playerId" + ref + "1"] = row.IdPlayer_A;
        match["playerName" + ref + "1"] = row.Player_A;
        match["playerPhoto" + ref + "1"] = row.UrlPlayer_A;
        match["playerId" + ref + "2"] = row.IdPlayer_B;
        match["playerName" + ref + "2"] = row.Player_B;
        match["playerPhoto" + ref + "2"] = row.UrlPlayer_B;
        var i = 1;
        while (i < 6 && row["Set" + i] != -1 && row["Set" + i] != null) {
        	console.log("id " + match.matchId);
            var s;
            if (match.sets.length >= i)
                s = match.sets[i - 1];
            else
                s = {};
            console.log(s);
            s[ref] = row["Set" + i];
            if (match.sets.length < i)
                match.sets.push(s);
            i++;
        }
    }
};

