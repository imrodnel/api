var express = require("express");
var https = require('https');
var http = require('http');
var fs = require('fs');
var config = require("./config");
var error = require("./error");
var cors = require("cors");
var bodyParser = require("body-parser");
var multer = require("multer");
var mysql = require("mysql");
var lwip = require("lwip");
var jwt = require("jwt-simple");
var converter = require('json-2-csv');
var nodeCache = require("node-cache");

var app = express();
if (config.httpPort) {
    http.createServer(app).listen(config.httpPort);
}
if (config.sslKeyPath && config.sslCertPath && config.httpsPort) {
    var options = {
        key: fs.readFileSync(config.sslKeyPath),
        cert: fs.readFileSync(config.sslCertPath)
    };
    https.createServer(options, app).listen(config.httpsPort);
}
app.set('case sensitive routing', true);
app.AClwip = lwip;
app.ACmysql = mysql;
app.ACconfig = config;
app.ACmulter = multer;
app.ACfs = fs;
app.ACconverter = converter;
app.ACcache = new nodeCache();

/**
 *
 * @param app
 * @param req
 * @param res
 * @param updateFunc updateFunc(req, res, sendResponse, updateCache)
 *              send_response : bool => true if the response must be sent
 *              updateCache(response) : the function must be called at the end of updateFunc to update the cache. reponse is the data to send.
 * @param delay Delay before cache update (in ms)
 * @constructor
 */
app.ACcachedCall = function (req, res, updateFunc, delay) {
    var c = app.ACcache.get(req.originalUrl)[req.originalUrl];
    if (!c) {
        //console.log("create cache : " + req.originalUrl);
        updateFunc(req, res, true, getUpdateCacheFunction(req.originalUrl));
    } else {
        //console.log("use cache : " + req.originalUrl);
        if ((new Date().getTime() - c.time) > delay) {
            //console.log("update cache : " + req.originalUrl);
            c.time += delay;
            app.ACcache.set(req.originalUrl, c, 0);
            updateFunc(req, res, false, getUpdateCacheFunction(req.originalUrl));
        }
        res.send(c.o);
    }
};

function getUpdateCacheFunction(key) {
    return function (o) {
        var c = {};
        c.time = new Date().getTime();
        c.o = o;
        //console.log("cache updated : " + key);
        app.ACcache.set(key, c, 0); // 0 => unlimited TTL
    }
}

var connection = mysql.createConnection({
    host: config.mysqlHost,
    user: config.mysqlUser,
    password: config.mysqlPassword,
    port: config.mysqlPort,
    database: config.mysqlDatabase,
    dateStrings: true,
    debug : true,
    trace : true
});
connection.connect();

app.use(cors());
app.use(bodyParser.json());
app.all('*', function (req, res, next) {
    res.header("Content-Type", "application/json");
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});

require("./auth.js")(app, connection, jwt);
require("./matches.js")(app, connection, error);
require("./supermatches.js")(app, connection);
require("./teams.js")(app, connection, error);
require("./courts.js")(app, connection, error);
require("./players.js")(app, connection, error);
require("./tournament.js")(app, connection, error);
require("./bracket.js")(app, connection);
require("./statistics.js")(app, connection, error);
require("./exports.js")(app, connection);


app.use(function (req, res) {
    res.status(404).json({code: "Erreur 404 - Tu peux toujours demander la page à la NSA"});
});
