module.exports = function (app, connection, error) {

    app.route("/tournament")
    /**
     * @api {get} /tournament Request infos about the tournament
     * @apiGroup Tournaments
     *
     * @apiSuccessExample Success-Response:
     {
        "code": 0,
        "tournament": {
            "Name": "MASTERU-TENNIS",
            "Phase": "Final"
        }
    }
     */
        .get(function (req, res) {
            connection.query("CALL GetTournament();", function (err, rows) {
                var o = {};
                if (err != null) {
                    o.code = -2;
                } else {
                    o.code = 0;
                    if (Array.isArray(rows))
                        o.tournament = rows[0][0];
                    else
                        o.tournament = {};
                }
                res.send(JSON.stringify(o, null, 4));
            });
        })

    /**
     * @api {post} /tournament
     * @apiGroup Tournaments
     * @apiParamExample Request-Example:
     {
    "name": "MASTER U 2015"
    }
     *
     */
        .post(function (req, res) {
            if (req.body.name) {
                //connection.query("CALL GetTournament();", function (err, rows) {
                var o = {};
                /*if (err != null) {
                 o.code = -2;
                 } else {
                 o.code = 0;
                 if (Array.isArray(rows))
                 o.tournament = rows[0][0];
                 else
                 o.tournament = {};
                 }*/
                res.send(JSON.stringify(o, null, 4));
            } else {
                error.error_message("Bad param(s)", res);
            }
        });

    app.route("/tournament/stage")
    /**
     * @api {get} /tournament/stage Get the stage of the tournament
     * @apiGroup Tournaments
     *
     * * @apiSuccessExample Success-Response:
     {
        "code": 0,
        "stage": Final
     }
     */
        .get(function (req, res) {
            connection.query("CALL GetTournament();", function (err, rows) {
                var o = {};
                if (err != null) {
                    o.code = -2;
                    o.message = "Database error";
                } else {
                    o.code = 0;
                    o.stage = rows[0][0].Stage;
                }
                res.send(JSON.stringify(o, null, 4));
            });
        })
    /**
     * @api {put} /tournament/stage Updates the stage of the tournament.
     * @apiGroup Tournaments
     ** @apiParamExample Request-Example:
     {
         "stage" = "1/2"
     }
     * @apiParam {string} stage SETTING-UP<br/>
     *                          1/4<br/>
     *                          1/2<br/>
     *                          Final<br/>
     *
     * @apiSuccessExample Success-Response:
     *
     {
        "code": 0
     }
     @apiSuccess {Integer} code
     0 : OK <br/>
     -2 : DB error<br/>
     -3 : Supermatch in progress<br/>
     */
        .put(function (req, res) {
            var o = {};
            if (req.body.stage) {
                connection.query("CALL UpdateStage(?);", [req.body.stage], function (err, rows) {
                    var o = {};
                    if (err == null) {
                        o.code = 0;
                    } else if (err.sqlState == 45000) {
                        o.code = -3;
                        o.message = "Supermatch in progress";
                    } else {
                        o.code = -2;
                    }
                    res.send(JSON.stringify(o, null, 4));
                });
            } else {
                o.code = -1;
                o.message = "Need param : 'stage'";
                res.send(JSON.stringify(o, null, 4));
            }
        });


    app.route("/tournament/name")
    /**
     * @api {put} /tournament/name Updates the name of the tournament.
     * @apiGroup Tournaments
     ** @apiParamExample Request-Example:
     {
         "name" = "Master-U 2022"
     }
     * @apiSuccessExample Success-Response:
     {
        "code": 0
     }
     */
        .put(function (req, res) {
            var o = {};
            if (req.body.name) {
                connection.query("CALL UpdateTournament(?);", [req.body.name], function (err, rows) {
                    var o = {};
                    if (err != null) {
                        o.code = -2;
                        o.message = "Database error";
                    } else {
                        o.code = 0;
                    }
                    res.send(JSON.stringify(o, null, 4));
                });
            } else {
                o.code = -1;
                o.message = "Need param : 'name'";
                res.send(JSON.stringify(o, null, 4));
            }
        });

    app.route("/tournament/tirage")
    /**
     * @api {put} /tournament/tirage Update the drawing
     * @apiGroup Tournaments
     * @apiParamExample Request-Example:
     {
        "team1": "FR",
        "team2": "FR2",
        "team3": "USA",
        "team4": "BEL",
        "team5": "USA2",
        "team6": "BEL2",
        "team7": "GER",
        "team8": "GER2"
     }
     * @apiSuccessExample Success-Response:
     {
        "code": 0,
        "tournament": {
            "Name": "MASTERU-TENNIS",
            "Phase": "Final"
        }
    }
     */
        .put(function (req, res) {
            var o = {};
            console.log('Parametres: ' + req.body);
            if (req.body.team1 &&
                req.body.team2 &&
                req.body.team3 &&
                req.body.team4 &&
                req.body.team5 &&
                req.body.team6 &&
                req.body.team7 &&
                req.body.team8) {
                // Params OK
                connection.query("CALL Tirage('" + req.body.team1 + "','" + req.body.team2 + "','" + req.body.team3 + "','"
                + req.body.team4 + "','" + req.body.team5 + "','" + req.body.team6 + "','" + req.body.team7 +
                "','" + req.body.team8 + "');", function (err, rows) {
                    if (err != null) {
                        o.code = -2;
                        console.log('ERROR: ' + err);
                    } else {
                        o.code = 0;
                        o.matches = rows[0];
                    }
                    res.send(JSON.stringify(o, null, 4));
                });
            } else {
                error.error_message("Bad param(s)", res);
            }
        })

    /**
     * @api {get} /tournament/tirage Get the drawing
     * @apiGroup Tournaments
     * @apiSuccessExample Success-Response:
     {
        "code": 0,
        "matches": [
            {
                "Team_A": "BEL",
                "Team_B": "CHN"
            },
            {
                "Team_A": "DEU",
                "Team_B": "FRA"
            },
              {
                  "Team_A": "GBR",
                "Team_B": "IRL"
            },
            {
                "Team_A": "RUS",
                "Team_B": "USA"
            }
        ]
    }
     */
        .get(function (req, res) {
            var o = {};
            connection.query("CALL GetTirage();", function (err, rows) {
                if (err != null) {
                    o.code = -2;
                    console.log('ERROR: ' + err);
                } else {
                    o.code = 0;
                    o.matches = rows[0];
                }
                res.send(JSON.stringify(o, null, 4));
            });

        });
};
