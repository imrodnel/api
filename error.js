module.exports = {
    error_mysql: function (err, res) {
        delete err.sqlState;
        delete err.index;
        res.status(500).json(err);
    }
    ,
    error_message: function (message, res) {
        res.status(500).json({error: message});
    }
}
;

