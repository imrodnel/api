module.exports = function (app, connection, jwt) {

    app.route("/login")
    /**
     * @api {post} /login Get a token
     * @apiGroup Authentification
     * @apiParamExample Request-Example:
     {
        "login": "Elon",
        "password": "Musk"
     }
     * @apiSuccessExample Success-Response:
     {
        "code": 0,
        "token": "xxxxxxxxxx..."
     }
     or
     {
        "code":-1/-2/-3
     }
     @apiSuccess {Integer} code
     O : <br/>
     -1 : <br/>
     -2 : <br/>
     -3 :
     */
        .post(function (req, res) {
            var o = {};
            if (req.body.login && req.body.password) {
                //params OK
                connection.query("CALL Login(?,?);", [req.body.login, req.body.password], function (err, rows) {
                    if (err != null) {
                        o.code = -2;
                    } else {
                        if (rows[0][0].success == "OK") {
                            o.code = 0;
                            o.token = generateToken(app, req.body.login);
                        }
                        else {
                            o.code = -3;
                        }
                    }
                    res.send(JSON.stringify(o, null, 4));
                });
            } else {
                o.code = -1;
                res.send(JSON.stringify(o, null, 4));
            }
        }
    );


    app.post("*", function (req, res, next) {
        checkToken(req, res, next);
    });
    app.put("*", function (req, res, next) {
        checkToken(req, res, next);
    });
    app.delete("*", function (req, res, next) {
        checkToken(req, res, next);
    });

    function generateToken(app, login) {
        return jwt.encode({
            "iss": login,
            "exp": Math.floor(Date.now() / 1000) + 604800 // 604800 = a week
        }, app.ACconfig.jwtPassword);
    }

    function checkToken(req, res, next) {
        var o = {};
        if (req.headers.ac_token) {
            //params OK
            try {
                var decoded = jwt.decode(req.headers.ac_token, app.ACconfig.jwtPassword);
                connection.query("CALL UserExists(?);", [decoded.iss], function (err, rows) {
                    if (err != null) {
                        o.code = -2;
                        res.send(JSON.stringify(o, null, 4));
                    } else {
                        if (rows[0][0].res == "1") {
                            next();
                        } else {
                            o.code = -98;
                            o.message = "user does not exists";
                            res.send(JSON.stringify(o, null, 4));
                        }
                    }
                });
            } catch (err) {
                o.code = -99;
                o.message = "bad token";
                res.send(JSON.stringify(o, null, 4));
            }
        } else {
            o.code = -1;
            o.message = "token needed";
            res.send(JSON.stringify(o, null, 4));
        }
    }

};
