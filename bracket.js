module.exports = function (app, connection) {

    app.route("/bracket")
    /**
     * @api {get} /bracket Get the bracket's data
     * @apiGroup Bracket
     *
     * @apiSuccessExample Success-Response:
     {
        "code": 0,
        "bracket": [
            {
                "IdHugeMatch": 1,
                "Team_A": "BEL",
                "Score_A": 5,
                "Team_B": "CHN",
                "Score_B": 2
            },
            {
                "IdHugeMatch": 2,
                "Team_A": "DEU",
                "Score_A": 6,
                "Team_B": "FRA",
                "Score_B": 1
            },
            {
                "IdHugeMatch": 3,
                "Team_A": "GBR",
                "Score_A": 1,
                "Team_B": "IRL",
                "Score_B": 6
            }
        ]
     }
     */
        .get(function (req, res) {
            connection.query("CALL InfoBracket();", function (err, rows) {
                var o = {};
                if (err != null) {
                    o.code = -2;
                } else {
                    o.code = 0;
                    if (Array.isArray(rows))
                        o.bracket = rows[0];
                    else
                        o.bracket = [];
                }
                res.send(JSON.stringify(o, null, 4));
            });
        })
};
