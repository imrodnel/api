Response format for all API calls :

    {
        "code" = XXX,
        "message" = "error description" [optional]
        // ....
    }

*code :*
* **0**     : OK
* **-1**    : Bad parameters
* **-2**    : Database error
* **-98**   : User does not exists(s) (token)
* **-99**   : Bad token(s)
* **other** : Depends on the API call

