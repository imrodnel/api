module.exports = function (app, connection, error) {

    app.route("/teams")
    /**
     * @api {get} /teams Request the list of nations
     * @apiGroup Teams
     *
     * @apiSuccess {String} NomNation Country code
     * @apiSuccess {Number} Points Points of the country. From 0 to 7
     *
     * @apiSuccessExample Success-Response:
     {
    "code": 0,
    "teams": [
        {
            "NameTeam": "BEL",
            "Points": 0,
            "URL": null
        },
        {
            "NameTeam": "CHN",
            "Points": 0,
            "URL": null
        }
        ]
     }
     */
        .get(function (req, res) {
            connection.query("SELECT * FROM `Teams`;", function (err, rows) {
                var o = {};
                if (err != null) {
                    o.code = -2;
                } else {
                    o.code = 0;
                    o.teams = rows;
                }
                res.send(JSON.stringify(o, null, 4));
            });
        });


    /**
     * @api {get} /teams/:nationCode Request informations for a country
     * @apiGroup Teams
     *
     * @apiParam {String} nationCode Team code (e.g. FRA).
     *
     * @apiSuccess {String} NomNation Country code
     * @apiSuccess {Number} Points Points of the country. From 0 to 7
     * @apiSuccessExample Success-Response:
     {
    "code": 0,
    "team": {
        "NameTeam": "FRA",
        "Points": 0,
        "URL": "http://upload.wikimedia.org/wikipedia/en/thumb/c/c3/Flag_of_France.svg/900px-Flag_of_France.svg.png"
    }
    }
     */
    app.route("/teams/:name([A-Z]+)")
        .get(function (req, res) {
            connection.query("SELECT * FROM `Teams` WHERE NameTeam = ?;", [req.params.name], function (err, rows) {
                var o = {};
                if (err != null) {
                    o.code = -2;
                } else if (rows.length > 0) {
                    o.code = 0;
                    o.team = rows[0];
                } else {
                    o.code = -1;
                }
                res.send(JSON.stringify(o, null, 4));
            });
        })


    app.route("/teams/:id")
    /**
     * @api {put} /teams/:name Updates a team
     * @apiGroup Teams
     * @apiParamExample Request-Example:
     {
         "token": "xxx..",
         "iso": "FRA",
         "name": "France",
         "url": "http://foo.com/french_flag.jpeg"
     }
     * @apiSuccessExample Success-Response:
     {"code":0}
     @apiSuccess {Integer} code
     0 : OK <br/>
     -2 : DB error<br/>
     -3 : team does not exist<br/>
     -4 : tournament in progress
     *
     */
        .put(function (req, res) {
            if (typeof req.body.iso != 'undefined' &&
                typeof req.body.name != 'undefined' &&
                typeof req.body.url != 'undefined') {
                connection.query("CALL UpdateTeam(?,?,?,?);", [req.params.id, req.body.name, req.body.iso, req.body.url]
                    , function (err, rows) {
                        var o = {};
                        if (err == null) {
                            o.code = 0;
                        } else if (err.sqlState == 45000) {
                            o.code = -3;
                            o.message = "Team does not exist"
                        } else {
                            o.code = -4;
                            o.message = "Tournament in progress"
                        }
                        res.send(JSON.stringify(o, null, 4));
                    });
            } else {
                var o = {};
                o.code = -1;
                o.message = "Bad params";
                res.send(JSON.stringify(o, null, 4));
            }
        })
    /**
     * @api {delete} /teams/:id Delete a team and its players
     * @apiGroup Teams
     *
     * @apiSuccessExample Success-Response:
     {"code":0}
     @apiSuccess {Integer} code
     0 : OK <br/>
     -2 : DB error<br/>
     -3 : tournament in progress<br/>
     *
     */
        .delete(function (req, res) {
            connection.query("CALL DeleteTeam(?);", [req.params.id], function (err, rows) {
                var o = {};
                if (err == null) {
                    o.code = 0;
                } else if (err.sqlState == 45000) {
                    o.code = -3;
                    o.message = "Tournament in progress"
                } else {
                    o.code = -2;
                }
                res.send(JSON.stringify(o, null, 4));
            });
        });
    ;

    /**
     * @api {get} /teams/:nationCode/players?[gender=M|W|X] Request the list of players of a nation
     * @apiGroup Teams
     *
     * @apiParam {String} [gender] Men, Women, All.
     *
     * @apiSuccessExample Success-Response:
     {
    "code": 0,
    "players": [
        {
            "IdPlayer": 46,
            "Team": "FRA",
            "Name": "BACQUIE",
            "FirstName": "Alice",
            "Sex": "W",
            "DateBirth": "1995-07-23"
        },
        {
            "IdPlayer": 47,
            "Team": "FRA",
            "Name": "GIRARD",
            "FirstName": "Rachel",
            "Sex": "W",
            "DateBirth": "1994-01-10"
        },
        {
            "IdPlayer": 48,
            "Team": "FRA",
            "Name": "LARRIERE",
            "FirstName": "Victoria",
            "Sex": "W",
            "DateBirth": "1991-05-02"
        }
    ]
    }
     */
    app.route("/teams/:name([A-Z]+)/players")
        .get(function (req, res) {
            // test if optional param "gender" exists. "X" is the default value.
            var gender = typeof req.query.gender === "undefined" ? 'X' : req.query.gender;
            connection.query("CALL ListsPlayersTeam('" + req.params.name + "','" + gender + "');", function (err, rows) {
                var o = {};
                if (err != null) {
                    o.code = -2;
                    o.message = "Database error";
                } else {
                    o.code = 0;
                    if (Array.isArray(rows))
                        o.players = rows[0];
                    else
                        o.players = [];
                }
                res.send(JSON.stringify(o, null, 4));
            });
        });

};
