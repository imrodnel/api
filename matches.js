module.exports = function (app, connection, error) {
    /**
     * @api {get} /matches/:id Information of match
     * @apiGroup Matchs
     *
     *  @apiParamExample Request-Example:
     *  /match/5
     *
     * @apiSuccessExample Success-Response:
     *
     {
   	 	"code": 0,
	    "match": [
    	    {
        	    "Team_A": "BEL",
	            "IdPlayerA_1": null,
    	        "IdPlayerA_2": null,
        	    "Name_CompletA": null,
	            "Team_B": "CHN",
    	        "IdPlayerB_1": null,
        	    "IdPlayerB_2": null,
	            "Name_CompletB": null,
    	        "Category": "DM",
        	    "Tableau": "1/4-P"
   	     }
    	]
	}
     @apiSuccess {Integer} code
     0 : OK <br/>
     -2 : DB error (with message field)<br/>
     */
    app.route("/matches/:id")
        .get(function (req, res) {
            var o = {};
            connection.query("CALL InfoMatch('" + req.params.id + "');", function (err, rows) {
                if (err != null) {
                    o.code = -2;
                    o.message = "Database error (bad round value)";
                } else {
                    o.code = 0;
                    if (Array.isArray(rows))
                        o.match = rows[0][0];
                    else
                        o.match = {};
                }
                res.send(JSON.stringify(o, null, 4));
            });
        });

    /**
     * @api {get} /matches?round=[1/4,1/2,W,CURRENT] Information of current of future matches
     * @apiGroup Matchs
     *
     *  @apiParamExample Request-Example:
     *  /matches?round=Final
     *
     * @apiSuccessExample Success-Response:
     *
     {
    "code": 0,
    "matches": [
        {
            "IdMatch": 57,
            "Team_A": null,
            "Name_Complet_A": null,
            "Team_B": null,
            "Name_Complet_B": null,
            "Category": "SM",
            "Court": null,
            "Tableau": "Final-P",
            "Statut": "SOON"
        },
        {
            "IdMatch": 68,
            "Team_A": null,
            "Name_Complet_A": null,
            "Team_B": null,
            "Name_Complet_B": null,
            "Category": "DM",
            "Court": null,
            "Tableau": "Final-3",
            "Statut": "SOON"
        },
        {
            "IdMatch": 69,
            "Team_A": null,
            "Name_Complet_A": null,
            "Team_B": null,
            "Name_Complet_B": null,
            "Category": "DW",
            "Court": null,
            "Tableau": "Final-3",
            "Statut": "SOON"
        },
        {
            "IdMatch": 84,
            "Team_A": null,
            "Name_Complet_A": null,
            "Team_B": null,
            "Name_Complet_B": null,
            "Category": "DX",
            "Court": null,
            "Tableau": "Final-7",
            "Statut": "SOON"
        }
    ]
}
     @apiSuccess {Integer} code
     0 : OK <br/>
     -2 : DB error (with message field)<br/>
     -3 : SETTING-UP (no matches) (with message field)<br/>
     */

    app.route("/matches")
        .get(function (req, res) {
            if (typeof req.query.round === "undefined") {
                var o = {};
                o.code = -1;
                o.message = "Require 'round' param";
                res.send(JSON.stringify(o, null, 4));
            } else {
                if (req.query.round == "CURRENT") {
                    connection.query("CALL GetTournament();", function (err, rows) {
                        if (rows[0][0].Stage != "SETTING-UP")
                            buildMatches(rows[0][0].Stage, res);
                        else {
                            var o = {};
                            o.code = -3;
                            o.message = "Current stage : SETTING-UP (no matches)";
                            res.send(JSON.stringify(o, null, 4));
                        }
                    })
                } else {
                    buildMatches(req.query.round, res);
                }
            }
        });

    function buildMatches(round, res) {
        var o = {};
        connection.query("CALL ListMatchs(?);", [round], function (err, rows) {
            if (err != null) {
                o.code = -2;
                o.message = "Database error (bad round value)";
            } else {
                o.code = 0;
                if (Array.isArray(rows))
                    o.matches = rows[0];
                else
                    o.matches = [];
            }
            res.send(JSON.stringify(o, null, 4));
        });
    }

    app.route("/matches/:id")
    /**
     * @api {put} /matches/:id Start, end of cancel a match
     * @apiGroup Matchs
     *
     *  @apiParamExample Request-Example:
     {
        "action": "START",
        "court": 5,
        "startDate": "2015-11-14 22:10:00",
        "service": 0,
        "playerAEq1Id": 4,
        "playerAEq2Id": 8,
        "playerBEq1Id": 0,
        "playerBEq2Id": 0
     }
     OR
     {
         "action": "END",
         "winner": "B",
         "endDate": "2015-11-14 23:30:00"
     }
     OR
     {
        "action": "ABANDON",
        "winner": "A",
        "endDate": "2015-11-14 23:30:00"
     }
     OR
     {
         "action": "CANCEL"
     }
     *
     * @apiSuccessExample Success-Response:
     {"code":0}
     *
     */
        .put(function (req, res) {
            var o = {};
            if (req.body.action == "START" &&
                typeof req.body.court != 'undefined' &&
                typeof req.body.startDate != 'undefined' &&
                typeof req.body.service != 'undefined' &&
                typeof req.body.playerAEq1Id != 'undefined' &&
                typeof req.body.playerAEq2Id != 'undefined' &&
                typeof req.body.playerBEq1Id != 'undefined' &&
                typeof req.body.playerBEq2Id != 'undefined') {
                var values = [req.params.id, req.body.court, req.body.startDate, req.body.service,
                    req.body.playerAEq1Id, req.body.playerBEq1Id, req.body.playerAEq2Id, req.body.playerBEq2Id];
                connection.query("CALL StartMatch(?,?,?,?,?,?,?,?);", values, function (err, rows) {
                        if (err == null) {
                            o.code = 0;
                        } else if(err.sqlState == 45002) {
                            o.code = 0; // todo change code
                            o.message = "match already started";
                        } else {
                            o.code = -2;
                        }
                        res.send(JSON.stringify(o, null, 4));
                    }
                );
            }
            else if ((req.body.action == "END" || req.body.action == "ABANDON") &&
                typeof req.body.winner != 'undefined' &&
                typeof req.body.endDate != 'undefined') {
                var values = [req.params.id, req.body.winner, req.body.endDate];
                connection.query("CALL EndMatch(?,?,?);", values, function (err, rows) {
                        if (err != null) {
                            o.code = -2;
                        } else {
                            o.code = 0;
                        }
                        res.send(JSON.stringify(o, null, 4));
                    }
                );
            }
            else if (req.body.action == "CANCEL") {
                connection.query("CALL CancelMatch(?);", [req.params.id], function (err, rows) {
                    if (err != null) {
                        o.code = -2;
                    } else {
                        o.code = 0;
                    }
                    res.send(JSON.stringify(o, null, 4));
                });
            } else {
                o.code = -1;
                res.send(JSON.stringify(o, null, 4));
            }
        }
    )
    ;

    app.route("/matches/:id/score")
    /**
     * @api {get} /matches/:id/score Score of a match
     * @apiGroup Matchs
     *
     *  @apiParamExample Request-Example:
     *  /matches/5/score
     *
     * @apiSuccessExample Success-Response:
     *
     {
        "code": 0,
        "sets": [
            {
                "A": 0,
                "B": 2
            }
        ],
        "scoreA": "0",
        "scoreB": "15"
    }
     @apiSuccess {Integer} code
     -3 : Match does not exist<br/>
     */
        .get(function (req, res) {
            connection.query("CALL GetMatchScore(?);", [req.params.id], function (err, rows) {
                var o = {};
                if (err == null) {
                    var row = rows[0][0];
                    o.code = 0;
                    o.sets = [];
                    o.scoreA = row.Score_A;
                    o.scoreB = row.Score_B;
                    var i = 1;
                    while (i < 6 && row["SetA" + i] != -1 && row["SetA" + i] != null) {
                        var s = {};
                        s.A = row["SetA" + i];
                        s.B = row["SetB" + i];
                        o.sets.push(s);
                        i++;
                    }
                } else if (err.sqlState == 45000) {
                    o.code = -3; // pointId doesn't exists
                    o.message = "match does not exist";
                } else {
                    o.code = -2;
                }
                res.send(JSON.stringify(o, null, 4));
            });
        })
    /**
     * @api {put} /matches/:matchId/score Changes the score and ends the match
     * @apiGroup Matchs
     *
     *  @apiParamExample Request-Example:
     {
        "winner": "B",
        "set1A": 4,
        "set1B": 6,
        "set2A": 4,
        "set2B": 6,
        "set3A": -1,
        "set3B": -1,
        "endDate": "2015-04-06 14:52:04"
     }
     *
     * @apiSuccessExample Success-Response:
     {"code":0}
     @apiSuccess {Integer} code
     0 : OK <br/>
     -1 : Bad parameters<br/>
     -2 : DB error<br/>
     *
     */
        .put(function (req, res) {
            var o = {};
            if (req.body.winner &&
                typeof req.body.set1A != 'undefined' &&
                typeof req.body.set1B != 'undefined' &&
                typeof req.body.set2A != 'undefined' &&
                typeof req.body.set2B != 'undefined' &&
                typeof req.body.set3A != 'undefined' &&
                typeof req.body.set3B != 'undefined' &&
                req.body.endDate) {
                connection.query("CALL AdminUpdateScore(?,?,?,?,?,?,?,?,?,?,?,?,?);", [req.params.id, req.body.winner,
                    req.body.set1A, req.body.set1B, req.body.set2A, req.body.set2B, req.body.set3A, req.body.set3B,
                    -1, -1, -1, -1, req.body.endDate], function (err, rows) {
                    if (err == null) {
                        o.code = 0;
                    } else {
                        o.code = -2;
                        o.message = "match not in progress";
                    }
                    res.send(JSON.stringify(o, null, 4));
                });
            }else {
                o.code = -1;
                res.send(JSON.stringify(o, null, 4));
            }
        });

    app.route("/matches/:id/score/:pointId")
    /**
     * @api {post} /matches/:id/score/:pointId Update the score of a match
     * @apiGroup Matchs
     *
     *  @apiParamExample Request-Example:
     *  /matches/5/score/59
     {
        "setNum": 5,
        "setA": 1,
        "setB": 0,
        "gameA": 4,
        "gameB": 5,
        "scoreA": 15,
        "scoreB": 40,
        "server": 1,
        "pointWinner": "A",
        "stats": "ACE",
        "fsf": 0,
        "break": 1
     }
     *
     * @apiSuccessExample Success-Response:
     {"code":0}
     @apiSuccess {Integer} code
     0 : OK <br/>
     -1 : Bad parameters<br/>
     -2 : DB error (with message field)<br/>
     -3 : pointId > oldPointId + 1 (with oldPointId field)<br/>
     -4 : pointId < oldPointId + 1 (with oldPointId field)<br/>
     -5 : Match not in progress<br/>
     *
     */
        .post(function (req, res) {
            var o = {};
            if (typeof req.params.pointId != 'undefined' &&
                typeof req.body.setNum != 'undefined' &&
                typeof req.body.setA != 'undefined' &&
                typeof req.body.setB != 'undefined' &&
                typeof req.body.gameA != 'undefined' &&
                typeof req.body.gameB != 'undefined' &&
                typeof req.body.scoreA != 'undefined' &&
                typeof req.body.scoreB != 'undefined' &&
                typeof req.body.server != 'undefined' &&
                typeof req.body.pointWinner != 'undefined' &&
                typeof req.body.stats != 'undefined' &&
                typeof req.body.fsf != 'undefined' &&
                typeof req.body.break != 'undefined') {
                //params OK
                connection.query("CALL IdMatchtoIdPoint(?)", [req.params.id], function (err, rows) {
                        if (err == null) {
                            var oldPointId = rows[0][0].maxPointId;
                            if (req.params.pointId == oldPointId + 1) {
                                var values = [req.params.pointId, req.params.id, req.body.setNum, req.body.setA, req.body.setB,
                                    req.body.gameA, req.body.gameB, req.body.scoreA, req.body.scoreB, req.body.server,
                                    req.body.pointWinner, req.body.stats, req.body.fsf, req.body.break];
                                connection.query("CALL UpdateTot(?,?,?,?,?,?,?,?,?,?,?,?,?,?);", values, function (err, rows) {
                                    if (err == null) {
                                        o.code = 0;
                                    } else if (err.sqlState == 45001) {
                                        o.code = -5;
                                        o.message = "Match not in progress";
                                    } else {
                                        o.code = -2;
                                        o.message = "UpdatePoint error";
                                    }
                                    res.send(JSON.stringify(o, null, 4));
                                });
                            } else if (req.params.pointId > oldPointId + 1) {
                                o.code = -3;
                                o.oldPointId = oldPointId;
                                res.send(JSON.stringify(o, null, 4));
                            } else if (req.params.pointId < oldPointId + 1) {
                                o.code = -4;
                                o.oldPointId = oldPointId;
                                res.send(JSON.stringify(o, null, 4));
                            }
                        } else {
                            o.code = -2;
                            o.message = "IdMatchToIdPoint error";
                            res.send(JSON.stringify(o, null, 4));
                        }
                    }
                );
            }
            else {
                o.code = -1;
                res.send(JSON.stringify(o, null, 4));
            }
        })
    /**
     * @api {delete} /matches/:id/score/:pointId Delete a point and all the higher points.
     * @apiGroup Matchs
     *
     *  @apiParamExample Request-Example:
     *  /matches/5/score/59
     *
     * @apiSuccessExample Success-Response:
     {"code":0}
     @apiSuccess {Integer} code
     0 : OK <br/>
     -1 : Bad parameters<br/>
     -2 : DB error (with message field)<br/>
     -3 : pointId doesn't exists
     *
     */
        .delete(function (req, res) {
            connection.query("CALL SupprPoint(?,?);", [req.params.id, req.params.pointId], function (err, rows) {
                var o = {};
                if (err == null) {
                    o.code = 0;
                } else if (err.sqlState == 45002) {
                    o.code = -3; // pointId doesn't exists
                } else {
                    o.code = -2;
                }
                res.send(JSON.stringify(o, null, 4));
            });
        });

    app.route("/matches/:id/set/:setId")
    /**
     * @api {put} /matches/:id/set/:setId Update the score of a set.
     * @apiGroup Matchs
     *
     *  @apiParamExample Request-Example:
     {
        "gameA": 4,
        "gameB": 5
     }
     *
     * @apiSuccessExample Success-Response:
     {"code":0}
     *
     */
        .put(function (req, res) {
            var o = {};
            if (typeof req.body.gameA != 'undefined' &&
                typeof req.body.gameB != 'undefined') {
                connection.query("CALL Update1SetDisp(?,?,?,?);", [req.params.id, req.params.setId, req.body.gameA, req.body.gameB],
                    function (err, rows) {
                        var o = {};
                        if (err == null) {
                            o.code = 0;
                        } else {
                            o.code = -2;
                        }
                        res.send(JSON.stringify(o, null, 4));
                    });
            } else {
                o.code = -1;
                res.send(JSON.stringify(o, null, 4));
            }
        });

    app.route("/matches/:matchId/court")
    /**
     * @api {put} /matches/:matchId/court Changes the court of a match
     * @apiGroup Matchs
     *
     *  @apiParamExample Request-Example:
     {
        "newCourt":2
     }
     *
     * @apiSuccessExample Success-Response:
     {"code":0}
     @apiSuccess {Integer} code
     0 : OK <br/>
     -1 : Bad parameters<br/>
     -2 : DB error (with message field)<br/>
     -3 : court unavailable<br/>
     -4 : match not in progress
     *
     */
        .put(function (req, res) {
            var o = {};
            if (typeof req.body.newCourt != 'undefined') {
                connection.query("CALL UpdateCourt(?,?);", [req.params.matchId, req.body.newCourt],
                    function (err, rows) {
                        if (err == null) {
                            o.code = 0;
                        } else if (err.sqlState == 45000) {
                            o.code = -3; // pointId doesn't exists
                            o.message = "court unavailable";
                        } else {
                            o.code = -4;
                            o.message = "match not in progress";
                        }
                        res.send(JSON.stringify(o, null, 4));
                    });
            } else {
                o.code = -1;
                res.send(JSON.stringify(o, null, 4));
            }
        });

    app.route("/matches/:matchId/players")
    /**
     * @api {put} /matches/:matchId/players Changes the players of a match (one by one)
     * @apiGroup Matchs
     *
     *  @apiParamExample Request-Example:
     {
        "oldPlayerId":2,
        "newPlayerId":9
     }
     *
     * @apiSuccessExample Success-Response:
     {"code":0}
     @apiSuccess {Integer} code
     0 : OK <br/>
     -1 : Bad parameters<br/>
     -2 : DB error (with message field)<br/>
     -3 : oldPlayerId doesn't play this match<br/>
     -4 : player not compatible
     *
     */
        .put(function (req, res) {
            var o = {};
            if (typeof req.body.oldPlayerId != 'undefined' &&
                typeof req.body.newPlayerId != 'undefined') {
                connection.query("CALL UpdatePlayerInMatch(?,?,?);", [req.params.matchId, req.body.oldPlayerId, req.body.newPlayerId],
                    function (err, rows) {
                        if (err == null) {
                            o.code = 0;
                        } else if (err.sqlState == 45001) {
                            o.code = -3; // pointId doesn't exists
                            o.message = "oldPlayerId doesn't play this match";
                        } else if (err.sqlState == 45002) {
                            o.code = -3; // pointId doesn't exists
                            o.message = "player not compatible";
                        } else {
                            o.code = -2;
                        }
                        res.send(JSON.stringify(o, null, 4));
                    });
            } else {
                o.code = -1;
                res.send(JSON.stringify(o, null, 4));
            }
        });
}
;
