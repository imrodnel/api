module.exports = function (app, connection) {

    /**
     * @api {get} /supermatches?round=[1/4,1/2,Final,CURRENT] Get the supermatches for a round
     * @apiGroup Supermatches
     *
     * @apiParam {string} round 1/4 : supermatchs des 1/4<br/>
     *                          1/2 : supermatchs des demis<br/>
     *                          W : supermatchs de la finale<br/>
     *                          CURRENT : supermatchs pour l'étape en cours du tournois<br/>
     *
     *  @apiParamExample Request-Example:
     *  /matches?round=CURRENT
     *
     * @apiSuccessExample Success-Response:
     *
     voir "https://gist.github.com/rakam/302d8a7b651843cca5df"
     */
    app.route("/supermatches")
        .get(function (req, res) {
            app.ACcachedCall(req, res, updateSupermatches, 3000);
        });

    function updateSupermatches(req, res, send, updateCache) {
        if (typeof req.query.round === "undefined") {
            var o = {};
            o.code = -1;
            o.message = "Require 'round' param";
            res.send(JSON.stringify(o, null, 4));
        } else {
            if (req.query.round == "CURRENT") {
                connection.query("CALL GetTournament();", function (err, rows) {
                    buildSupermatches(req, res, rows[0][0].Stage, send, updateCache);
                })
            } else {
                buildSupermatches(req, res, req.query.round, send, updateCache);
            }
        }
    }

    function buildSupermatches(req, res, round, send, updateCache) {
        var o = {};
        connection.query("CALL InfosHugeMatchs(?);", [round], function (err, rows) {
            if (err != null) {
                o.code = -2;
                o.message = "Database error (bad round value)";
            } else {
                o.code = 0;
                o.supermatches = [];
                if (Array.isArray(rows))
                    buildResponses(o.supermatches, rows[0]);
            }

            var stringifiedO = JSON.stringify(o, null, 4);
            updateCache(stringifiedO);
            if (send)
                res.send(stringifiedO);
        });
    }

    function buildResponses(supermatches, rows) {
        for (var i = 0; i < rows.length; i++) {
            var row = rows[i];
            var superId = row.IdHugeMatch;
            var index = (superId - 1) % 4;
            if (supermatches.length <= index)
                addSupermatch(supermatches, row);
            addMatch(supermatches[index], row);
        }
    }

    function addSupermatch(supermatches, row) {
        var supermatch = {};
        supermatch.id = row.IdHugeMatch;
        supermatch.teamISOA = row.Team_A;
        supermatch.teamISOB = row.Team_B;
        supermatch.teamNameA = row.NameTeam_A;
        supermatch.teamNameB = row.NameTeam_B;
        supermatch.teamFlagA = row.UrlTeam_A;
        supermatch.teamFlagB = row.UrlTeam_B;
        supermatch.teamScoreA = 0;
        supermatch.teamScoreB = 0;
        supermatch.tableau = row.Tableau;
        supermatch.matches = [];
        supermatches.push(supermatch);
    }

    function addMatch(supermatches, row) {
        var match = {};
        match.id = row.IdMatch;
        match.status = row.Statut;
        match.category = row.Category;
        match.court = row.Court;
        match.playerIdA1 = row.IdPlayer_A1;
        match.playerIdA2 = row.IdPlayer_A2;
        match.playerIdB1 = row.IdPlayer_B1;
        match.playerIdB2 = row.IdPlayer_B2;
        match.playerNameA1 = row.Player_A1;
        match.playerNameA2 = row.Player_A2;
        match.playerNameB1 = row.Player_B1;
        match.playerNameB2 = row.Player_B2;
        match.playerPhotoA1 = row.UrlPlayer_A1;
        match.playerPhotoA2 = row.UrlPlayer_A2;
        match.playerPhotoB1 = row.UrlPlayer_B1;
        match.playerPhotoB2 = row.UrlPlayer_B2;
        match.scoreA = row.Score_A;
        match.scoreB = row.Score_B;
        match.service = row.ServiceA == 1 ? "A" : "B";
        match.winner = row.Winner;
        match.sets = [];
        var i = 1;
        while (i < 6 && row["SetA" + i] != -1 && row["SetA" + i] != null) {
            var s = {};
            s.A = row["SetA" + i];
            s.B = row["SetB" + i];
            match.sets.push(s);
            i++;
        }
        if (match.winner == "A")
            supermatches.teamScoreA++;
        if (match.winner == "B")
            supermatches.teamScoreB++;
        supermatches.matches.push(match);
    }

}
;