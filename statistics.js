module.exports = function (app, connection, error) {

    app.route("/statistics/matches/:matchId")
    /**
     * @api {get} /statistics/matches/:matchId Get the stats for the 2 teams of a match
     * @apiGroup Statistics
     *
     * @apiSuccessExample Success-Response:
     {
         "code": 0,
         "statistics": {
             "A": {
                 "Ref": "A",
                 "NbAce": 1,
                 "NbWonServe": 0,
                 "NbWinnerPoint": 0,
                 "NbDoubleFault": 0,
                 "NbUnforcedError": 0,
                 "NbForcedError": 0,
                 "NbFirstServeFault": 1,
                 "NbBreak": 1,
                 "NbWonBreak": 1,
                 "NbServe": 8,
                 "NbSuccessfulFirstServe": 6
             },
             "B": {
                 "Ref": "B",
                 "NbAce": 1,
                 "NbWonServe": 0,
                 "NbWinnerPoint": 1,
                 "NbDoubleFault": 1,
                 "NbUnforcedError": 2,
                 "NbForcedError": 0,
                 "NbFirstServeFault": 0,
                 "NbBreak": 1,
                 "NbWonBreak": 1,
                 "NbServe": 5,
                 "NbSuccessfulFirstServe": 5
             },
             "IdMatch": 1
         }
     }
     */
        .get(function (req, res) {
            app.ACcachedCall(req, res, updateStatsCourtsCache, 15000);
        });

    function updateStatsCourtsCache(req, res, send, updateCache) {
        var o = {};
        connection.query("CALL GetStatMatch(?);", [req.params.matchId], function (err, rows) {
            if (err != null) {
                o.code = -2;
            } else {
                o.code = 0;
                if (Array.isArray(rows[0]) && rows[0].length == 2) {
                    var stats = {};
                    stats.IdMatch = rows[0][0].IdMatch;
                    stats[rows[0][0].Ref] = {};
                    buildStats(stats[rows[0][0].Ref], rows[0][0]);
                    stats[rows[0][1].Ref] = {};
                    buildStats(stats[rows[0][1].Ref], rows[0][1]);
                    o.statistics = stats;
                }
                else
                    o.statistics = [];
            }
            var stringfiedO = JSON.stringify(o, null, 4);
            updateCache(stringfiedO);
            if (send)
                res.send(stringfiedO);
        });
    }

    function buildStats(stats, row) {
        stats["Ref"] = row.Ref;
        stats["NbAce"] = row.NbAce;
        stats["NbWonServe"] = row.NbWonServe;
        stats["NbWinnerPoint"] = row.NbWinnerPoint;
        stats["NbDoubleFault"] = row.NbDoubleFault;
        stats["NbUnforcedError"] = row.NbUnforcedError;
        stats["NbForcedError"] = row.NbForcedError;
        stats["NbFirstServeFault"] = row.NbFirstServeFault;
        stats["NbBreak"] = row.NbBreak;
        stats["NbWonBreak"] = row.NbWonBreak;
        stats["NbServe"] = row.NbServe;
        stats["NbSuccessfulFirstServe"] = row.NbSuccessfulFirstServe;
        stats["NbRightToLeftAsServer"] = row.NbRightToLeftAsServer;
        stats["NbRightToLeftWonAsServer"] = row.NbRightToLeftWonAsServer;
        stats["NbLeftToRightAsServer"] = row.NbLeftToRightAsServer;
        stats["NbLeftToRightWonAsServer"] = row.NbLeftToRightWonAsServer;
        stats["NbRightToLeftAsReceiver"] = row.NbRightToLeftAsReceiver;
        stats["NbRightToLeftWonAsReceiver"] = row.NbRightToLeftWonAsReceiver;
        stats["NbLeftToRightAsReceiver"] = row.NbLeftToRightAsReceiver;
        stats["NbLeftToRightWonAsReceiver"] = row.NbLeftToRightWonAsReceiver;
    }

    app.route("/statistics/players/:playerId")
    /**
     * @api {get} /statistics/players/:playerId Player's statistics into the tournament
     * @apiGroup Statistics
     *
     * @apiSuccessExample Success-Response:
     {
    "code": 0,
    "statistics": {
        "Name": "Jonas MERCKX",
        "IdPlayer": 3,
        "NbMatches": 2,
        "NbAce": 1,
        "NbWonServe": 0,
        "NbWinnerPoint": 0,
        "NbDoubleFault": 0,
        "NbUnforcedError": 0,
        "NbForcedError": 0,
        "NbFirstServeFault": 3,
        "NbBreak": 2,
        "NbWonBreak": 2,
        "NbServe": 24,
        "NbSuccessfulFirstServe": 20
    }
    }
     */
        .get(function (req, res) {
            connection.query("CALL GetStatPlayer(" + req.params.playerId + ");", function (err, rows) {
                var o = {};
                if (err != null) {
                    o.code = -2;
                } else {
                    o.code = 0;
                    if (Array.isArray(rows))
                        o.statistics = rows[0][0];
                    else
                        o.statistics = [];
                }
                res.send(JSON.stringify(o, null, 4));
            });
        });
};

