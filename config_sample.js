var config = {};

// Port used by the unsecure api
// Set to null to disable the unsecure api
config.httpPort = 5001;

// Port used by the secure api
// Set to null to disable the secure api
config.httpsPort = 5002;

// Path to the private key file (must be specified if the secure api is used)
config.sslKeyPath = "/ssl/key_api.pem";

// Path to the SSL certificate (must be specified if the secure api is used)
config.sslCertPath = "/ssl/cert_api.pem";

// Adress of the MySQL server
config.mysqlHost = "localhost";

// MySQL username used by the api
config.mysqlUser = "api";

// MySQL password used by the api
config.mysqlPassword = "******secret******";

// MySQL port used by the api
config.mysqlPort = "3232";

// MySQL database's name used by the api
config.mysqlDatabase = "MASTERU-TENNIS";

// String used by the api to encrypt the token given after an authentification
config.jwtPassword = "long_random_string";

// Path to the players photos directory (must be ended by a /)
// The api must have the read and write rights in it
config.photos_directory_path = "/var/www/html/photos/";

// Url to the players photos directory (musb be ended by a /)
// The file in it must be accessible with the <config.photos_directory_url>/file_name url
config.photos_directory_url = "http://your-adress.com/photos/";

module.exports = config;

/* Token test
 eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJwZXJjZXZhbCIsImV4cCI6MTQyODMzMDMwNn0.2gG9WCQvHSki-GyU6UwlICYNk46AnBTxgd5RLctgwjA
 */