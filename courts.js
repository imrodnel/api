module.exports = function (app, connection, error) {

    app.route("/courts/infos")
    /**
     * @api {get} /courts/infos Request the informations about all the courts
     * @apiGroup Courts
     *
     * @apiSuccessExample Success-Response:
     {
     "code": 0,
     "courts": {
         "1": {
             "set": [
                 {
                     "A": 0,
                     "B": 1
                 }
             ],
             "teamA": "FR",
             "teamFlagA": "http://5.196.21.161/drapeaux/fr.png",
             "playerIdA1": 7,
             "playerIdA2": 11,
             "playerNameA1": "jkqsnbq kjqsdbqjs",
             "playerNameA2": "Az ERTY",
             "playerPhotoA1": "http://5.196.21.161/photos/photo_player_7.png",
             "playerPhotoA2": "http://5.196.21.161/photos/photo_player_11.png",
             "scoreA": "0",
             "service": "A",
             "duration": "02:27",
             "winner": null,
             "tableau": "1/4-P",
             "category": "DW",
             "court": 1,
             "idMatch": 6,
             "teamB": "AR",
             "teamFlagB": "http://5.196.21.161/drapeaux/ar.png",
             "playerIdB1": 12,
             "playerIdB2": 8,
             "playerNameB1": "Qwer TY",
             "playerNameB2": "arg kjqsbds",
             "playerPhotoB1": "http://5.196.21.161/photos/photo_player_12.jpeg",
             "playerPhotoB2": "http://5.196.21.161/photos/photo_player_8.png",
             "scoreB": "15"
         }
     }
    }
     */
        .get(function (req, res) {
            app.ACcachedCall(req, res, updateCourtsCache, 2000);
        });

    function updateCourtsCache(req, res, send, updateCache) {
        var o = {};
        connection.query("CALL InfosOfCourt();", function (err, rows) {
            if (err != null) {
                o.code = -2;
            } else {
                o.code = 0;
                if (Array.isArray(rows)) {
                    var courts = {};
                    for (var j = 0; j < rows.length - 1; j++) {
                        var arrayLength = rows[j].length;
                        for (var i = 0; i < arrayLength; i++) {
                            var row = rows[j][i];
                            if (row.Court in courts) {
                                addCourtInfo(courts[row.Court], row);
                            } else {
                                courts[row.Court] = {};
                                courts[row.Court]["set"] = [];
                                addCourtInfo(courts[row.Court], row);
                            }
                        }
                    }
                    o.courts = courts;
                }
                else {
                    o.courts = {};
                }
            }
            var stringfiedO = JSON.stringify(o, null, 4);
            updateCache(stringfiedO);
            if (send)
                res.send(stringfiedO);
        });
    }

    app.route("/courts/available")
    /**
     * @api {get} /courts/available Get the list of available courts
     * @apiGroup Courts
     *
     * @apiSuccessExample Success-Response:
     {
     "code": 0,
     "courts": [
         1,
         2,
         3,
         4,
         5,
         7
     ]
     }
     */
        .get(function (req, res) {
            var o = {};
            connection.query("CALL GetCourt();", function (err, rows) {
                if (err != null) {
                    o.code = -2;
                } else {
                    o.code = 0;
                    o.courts = [];
                    if (Array.isArray(rows) && Array.isArray(rows[0])) {
                        var len = rows[0].length;
                        for (var i = 0; i < len; i++) {
                            o.courts.push(rows[0][i].Court);
                        }
                    }
                }
                res.send(JSON.stringify(o, null, 4));
            });
        });

    app.route("/courts/infos/:id")
    /**
     * @api {get} /courts/infos/:id Get the informations about the court specified (:id)
     * @apiGroup Courts
     *
     * @apiSuccessExample Success-Response:
     {
    "code": 0,
    "court": {
        "set": [
            {
                "A": 0,
                "B": 1
            }
        ],
        "teamA": "FR",
        "teamFlagA": "http://5.196.21.161/drapeaux/fr.png",
        "playerIdA1": 7,
        "playerIdA2": 11,
        "playerNameA1": "jkqsnbq kjqsdbqjs",
        "playerNameA2": "Az ERTY",
        "playerPhotoA1": "http://5.196.21.161/photos/photo_player_7.png",
        "playerPhotoA2": "http://5.196.21.161/photos/photo_player_11.png",
        "scoreA": "0",
        "service": "A",
        "duration": "02:28",
        "winner": null,
        "tableau": "1/4-P",
        "category": "DW",
        "court": 1,
        "idMatch": 6,
        "teamB": "AR",
        "teamFlagB": "http://5.196.21.161/drapeaux/ar.png",
        "playerIdB1": 12,
        "playerIdB2": 8,
        "playerNameB1": "Qwer TY",
        "playerNameB2": "arg kjqsbds",
        "playerPhotoB1": "http://5.196.21.161/photos/photo_player_12.jpeg",
        "playerPhotoB2": "http://5.196.21.161/photos/photo_player_8.png",
        "scoreB": "15"
    }
    }
     */
        .get(function (req, res) {
            app.ACcachedCall(req, res, updateCourtCache, 3000);
        });

    function updateCourtCache(req, res, send, updateCache) {
        var o = {};
        connection.query("CALL InfoCourt(?);", [req.params.id], function (err, rows) {
            if (err != null) {
                o.code = -2;
                o.message = err.sqlState;
            } else {
                o.code = 0;
                o.court = {};
                o.court["set"] = [];
                addCourtInfo(o.court, rows[0][0]);
                addCourtInfo(o.court, rows[0][1]);
            }
            var stringfiedO = JSON.stringify(o, null, 4);
            updateCache(stringfiedO);
            if (send)
                res.send(stringfiedO);
        });
    }


    function addCourtInfo(court, row) {
        var ref = row.Ref;
        court["team" + ref] = row.Team;
        court["teamFlag" + ref] = row.UrlTeam;
        court["playerId" + ref + "1"] = row.IdPlayer_A;
        court["playerId" + ref + "2"] = row.IdPlayer_B;
        court["playerName" + ref + "1"] = row.Player_A;
        court["playerName" + ref + "2"] = row.Player_B;
        court["playerPhoto" + ref + "1"] = row.UrlPlayer_A;
        court["playerPhoto" + ref + "2"] = row.UrlPlayer_B;
        court["score" + ref] = row.Score;
        if (row.Service == 1)
            court["service"] = ref;
        court["duration"] = row.Duree;
        court["winner"] = row.Winner;
        court["tableau"] = row.Tableau;
        court["category"] = row.Category;
        court["court"] = row.Court;
        court["idMatch"] = row.IdMatch;
        for (var i = 1; i <= 5; i++) {
            if (row["Set" + i] != null) {
                if (court.set.length < i) {
                    court.set.push({});
                }
                court.set[i - 1][ref] = row["Set" + i];
            }
        }
    }
};